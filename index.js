const fs = require('fs');
const Discord = require('discord.js');
const { prefix, token } = require('./config.json');


const client = new Discord.Client();
client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}

client.once('ready', () => {
	console.log('Ready!');
    client.user.setPresence({
        status: "Online",  //Online, Idle DND
    });
    client.user.setActivity(`Prefix "` + prefix + `" | HelperBot!`, { type: 'LISTENING' });

});

function checkBans()
{
    x = 5;  // 5 Seconds
   console.log("5 seconds!");
    setTimeout(checkBans, x*1000);
}


checkBans();

client.on('message', message => {
	if (!message.content.startsWith(prefix) || message.author.bot || !message.guild) return;

    const args = message.content.slice(prefix.length).trim().split(/ +/);
	const command = args.shift().toLowerCase();
	try {
		client.commands.get(command).execute(message, args, Discord);
	} catch (error) {
		console.error(error);
        const errorEmbed = new Discord.MessageEmbed()
            .setColor('#ff0000')
            .setTitle("Error")
            .setDescription("Command not found. Try >help");
        message.reply(errorEmbed)
        .catch(err => {
            console.error(err);
        });

	}
});

client.login(token);
