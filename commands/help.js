module.exports = {
	name: 'help',
	description: 'Help!',
	execute(message, args, Discord) {
        console.log(message.author.tag + " Executed >help in " + message.guild.name);
        const embed = new Discord.MessageEmbed()
            .setTitle("Bot Commands")
            .addFields(
                { name: '>ban [USER]', value: 'Bans a user from the server.' },
                { name: '>kick [USER]', value: 'Kicks a user from the server.' },
                { name: '>avatar [USER]', value: 'Gets a users avatar.' }
            )
            .setThumbnail('https://cdn.discordapp.com/attachments/557336121349111859/803846716130459698/390dde81-e1b3-40de-8d6b-57f63638677d_200x200.png')
            .setTimestamp()
            .setFooter('Help Command', 'https://cdn.discordapp.com/attachments/557336121349111859/803846716130459698/390dde81-e1b3-40de-8d6b-57f63638677d_200x200.png');
        message.channel.send(embed)
        .catch(err => {
            console.error(err);
        });
    },
};
