module.exports = {
	name: 'kick',
	description: 'Kicks a member from the guild',
	execute(msg, args, Discord) {
    if (!msg.member.hasPermission("KICK_MEMBERS")) {
            const errorEmbed = new Discord.MessageEmbed()
                .setColor('#ff0000')
                .setTitle("Error")
                .setDescription("You don't have permission!");
            msg.reply(errorEmbed)
            .catch(err => {
                console.error(err);
            });
            return;
        }
        console.log(msg.author.tag + " Executed >kick in " + msg.guild.name);
        if (!msg.mentions.members.first()) {
            const errorEmbed = new Discord.MessageEmbed()
                .setColor('#ff0000')
                .setTitle("Error")
                .setDescription("You didn't mention a user!");
            msg.reply(errorEmbed)
            .catch(err => {
                console.error(err);
            });
            return;
        }
        const user = msg.mentions.users.first();
        if (user) {
            const member = msg.guild.member(user);
            if (member) {
                member.kick('Kicked by HelperBot.')
                .then(() => {
                    const successEmbed = new Discord.MessageEmbed()
                        .setColor('#4dfc02')
                        .setTitle("Success!")
                        .setDescription(user.tag + " was kicked from the server.");
                    msg.reply(successEmbed)
                    .catch(err => {
                        console.error(err);
                    });

                })
                .catch(err => {
                    const errorEmbed1 = new Discord.MessageEmbed()
                    .setColor('#ff0000')
                    .setTitle("Error")
                    .setDescription("An error occured! (Do I have the correct permissions?)");
                msg.reply(errorEmbed1)
                .catch(err => {
                    console.error(err);
                });
                return;
                });
            }
            else {
                const errorEmbed = new Discord.MessageEmbed()
                    .setColor('#ff0000')
                    .setTitle("Error")
                    .setDescription("That user isn't in the server!");
                msg.reply(errorEmbed)
                .catch(err => {
                    console.error(err);
                });
                return;
            }
        }
        else {
            const errorEmbed = new Discord.MessageEmbed()
                .setColor('#ff0000')
                .setTitle("Error")
                .setDescription("An Error Occured! (Do I have the correct permissions?)");
            msg.reply(errorEmbed)
                .catch(err => {
                    console.error(err);
                });

            return;
        }
    },
};
