module.exports = {
	name: 'avatar',
	description: 'shows useres avatar',
	execute(msg, args, Discord) {
        if (!msg.mentions.members.first()) {
                const errorEmbed = new Discord.MessageEmbed()
                    .setColor('#ff0000')
                    .setTitle("Error")
                    .setDescription("You didn't mention a user!");
                msg.reply(errorEmbed)
                .catch(err => {
                    console.error(err);
                });
                return;
            }
            const user = msg.mentions.users.first();
            msg.reply("https://cdn.discordapp.com/avatars/" + user.id + "/" + user.avatar + ".png")
            .catch(err => {
                console.error(err);
            });
            console.log(msg.author.tag + " Executed >avatar in " + msg.guild.name);
        },
    };

