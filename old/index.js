const Discord = require('discord.js');
const client = new Discord.Client();


client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
    client.user.setPresence({
        status: "Online",  //Online, Idle DND
    });
    client.user.setActivity(`Prefix ">" | HelperBot!`, { type: 'LISTENING' });
});

client.on('message', msg => {
    if (!msg.guild) return;
    if (msg.author.bot) return;
    // Help Command //
    if (msg.content === '>help') {
        console.log(msg.author.tag + " Executed >help in " + msg.guild.name);
        const embed = new Discord.MessageEmbed()
        .setTitle("Bot Commands")
        .addFields(
            { name: '>ban [USER]', value: 'Bans a user from the server.' },
            { name: '>kick [USER]', value: 'Kicks a user from the server.' },
            { name: '>avatar [USER]', value: 'Gets a users avatar.' }
        )
        .setThumbnail('https://cdn.discordapp.com/attachments/557336121349111859/803846716130459698/390dde81-e1b3-40de-8d6b-57f63638677d_200x200.png')
        .setTimestamp()
        .setFooter('Help Command', 'https://cdn.discordapp.com/attachments/557336121349111859/803846716130459698/390dde81-e1b3-40de-8d6b-57f63638677d_200x200.png');
        msg.channel.send(embed)
        .catch(err => {
            console.error(err);
        });
    }
    // Kick Command //
    if (msg.content.startsWith('>kick')) {
        if (!msg.member.hasPermission("KICK_MEMBERS")) {
            const errorEmbed = new Discord.MessageEmbed()
                .setColor('#ff0000')
                .setTitle("Error")
                .setDescription("You don't have permission!");
            msg.reply(errorEmbed)
            .catch(err => {
                console.error(err);
            });
            return;
        }
        console.log(msg.author.tag + " Executed >kick in " + msg.guild.name);
        if (!msg.mentions.members.first()) {
            const errorEmbed = new Discord.MessageEmbed()
                .setColor('#ff0000')
                .setTitle("Error")
                .setDescription("You didn't mention a user!");
            msg.reply(errorEmbed)
            .catch(err => {
                console.error(err);
            });
            return;
        }
        const user = msg.mentions.users.first();
        if (user) {
            const member = msg.guild.member(user);
            if (member) {
                member.kick('Kicked by HelperBot.')
                .then(() => {
                    const successEmbed = new Discord.MessageEmbed()
                        .setColor('#4dfc02')
                        .setTitle("Success!")
                        .setDescription(user.tag + " was kicked from the server.");
                    msg.reply(successEmbed)
                    .catch(err => {
                        console.error(err);
                    });

                })
                .catch(err => {
                    const errorEmbed1 = new Discord.MessageEmbed()
                    .setColor('#ff0000')
                    .setTitle("Error")
                    .setDescription("An error occured! (Do I have the correct permissions?)");
                msg.reply(errorEmbed1)
                .catch(err => {
                    console.error(err);
                });
                return;
                });
            }
            else {
                const errorEmbed = new Discord.MessageEmbed()
                    .setColor('#ff0000')
                    .setTitle("Error")
                    .setDescription("That user isn't in the server!");
                msg.reply(errorEmbed)
                .catch(err => {
                    console.error(err);
                });
                return;
            }
        }
        else {
            const errorEmbed = new Discord.MessageEmbed()
                .setColor('#ff0000')
                .setTitle("Error")
                .setDescription("An Error Occured! (Do I have the correct permissions?)");
            msg.reply(errorEmbed)
                .catch(err => {
                    console.error(err);
                });

            return;
        }
    }
    // Ban Command //
    if (msg.content.startsWith('>ban')) {
        if (!msg.member.hasPermission("BAN_MEMBERS")) {
            const errorEmbed = new Discord.MessageEmbed()
                .setColor('#ff0000')
                .setTitle("Error")
                .setDescription("You don't have permission!");
            msg.reply(errorEmbed)
            .catch(err => {
                console.error(err);
            });
            return;
        }
        console.log(msg.author.tag + " Executed >ban in " + msg.guild.name);
        if (!msg.mentions.members.first()) {
            const errorEmbed = new Discord.MessageEmbed()
                .setColor('#ff0000')
                .setTitle("Error")
                .setDescription("You didn't mention a user!");
            msg.reply(errorEmbed)
            .catch(err => {
                console.error(err);
            });
            return;
        }
        const user = msg.mentions.users.first();
        if (user) {
            const member = msg.guild.member(user);
            if (member) {
                member.ban({
                    reason: 'Banned by HelperBot.'
                })
                .then(() => {
                    const successEmbed = new Discord.MessageEmbed()
                        .setColor('#4dfc02')
                        .setTitle("Success!")
                        .setDescription(user.tag + " was banned from the server.");
                    msg.reply(successEmbed)
                    .catch(err => {
                        console.error(err);
                    });
                })
                .catch(err => {
                    const errorEmbed1 = new Discord.MessageEmbed()
                        .setColor('#ff0000')
                        .setTitle("Error")
                        .setDescription("An error occured! (Do I have the correct permissions?)");
                    msg.reply(errorEmbed1)
                    .catch(err => {
                        console.error(err);
                    });
                    return;
                });
            }
            else {
                const errorEmbed1 = new Discord.MessageEmbed()
                    .setColor('#ff0000')
                    .setTitle("Error")
                    .setDescription("That user isn't in the server!");
                msg.reply(errorEmbed1)
                .catch(err => {
                    console.error(err);
                });
                return;
            }
        }
        else {
            const errorEmbed1 = new Discord.MessageEmbed()
                .setColor('#ff0000')
                .setTitle("Error")
                .setDescription("An error occured! (Do I have the correct permissions?)");
            msg.reply(errorEmbed1)
            .catch(err => {
                console.error(err);
            });
            return;
            }
    }

    
    // Avatar Command //
    if (msg.content.startsWith('>avatar')) {
        if (!msg.mentions.members.first()) {
            const errorEmbed = new Discord.MessageEmbed()
                .setColor('#ff0000')
                .setTitle("Error")
                .setDescription("You didn't mention a user!");
            msg.reply(errorEmbed);
            return;
        }
        const user = msg.mentions.users.first();
        msg.reply("https://cdn.discordapp.com/avatars/" + user.id + "/" + user.avatar + ".png")
        .catch(err => {
            console.error(err);
        });
        console.log(msg.author.tag + " Executed >avatar in " + msg.guild.name);
    }
});

client.login('Token')
