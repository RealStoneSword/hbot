# Helper Bot

HelperBot, AKA HBot, is a simple, easy-to-use discord bot. It has many features and is currently in a work-in-progress state.

## Currently suppourted commands

\>help

\>avatar

\>kick

\>ban

## Work In Progress Commands

None currently.

Suggestions are welcome and encouraged! (make an issue if you have a suggestion)

## Running

You will need to install a few things.
Make sure you have ```npm``` and ```node.js```
First, clone the repo.
CD into the repo's folder and install ```discord.js``` using ```npm install discord.js```.
Then, edit the bottom line of ```index.js``` and insert your bot token.
Finally, run using ```node index.js```.
